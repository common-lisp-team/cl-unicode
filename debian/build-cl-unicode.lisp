;;;
;;; Load the cl-unicode/build system to generate some lisp source files
;;;

(require :asdf)
(let ((asdf:*central-registry* (list *default-pathname-defaults*))
      (asdf:*user-cache* (merge-pathnames #p"debian/cache/")))
  (asdf:load-system :cl-unicode/build))
